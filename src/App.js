import React, { Component } from 'react';
import Form from './components/Form'
import DisplayForm from './components/DisplayForm'
import MyTable from './components/MyTable'
import 'react-table/react-table.css'
import './styles.css'


export default class App extends Component {
  render() {
    return (
      <div>
        <h1 className='title'>Hello, world.</h1>
        <Form />
        <DisplayForm />
        <MyTable />
      </div>
    );
  }
}
