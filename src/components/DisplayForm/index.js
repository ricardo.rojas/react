import React from 'react'
import { saveForm } from '../../reducers/Form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const mapStateToProps = (state) => {
  return {
      name : state.formReducer.nombre,
      apellido: state.formReducer.apellido,
      dirrecion: state.formReducer.dirrecion,
      
  }
}

class DisplayForm extends React.Component {
    
    render(){
      const {
        name,
        apellido,
        dirrecion
      } = this.props
      
      return (
        <div>
          <label>Nombre: {name}</label>
          <br/>
          <label>apellido: {apellido}</label>
          <br/>
          <label>dirrecion: {dirrecion}</label>
        </div>
      )
    }
}


export default connect(mapStateToProps,null)(DisplayForm)