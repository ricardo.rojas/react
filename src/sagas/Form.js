import axios from  'axios'
import { takeLatest, put } from 'redux-saga/effects'
import { delay  } from 'redux-saga'
import { postFormSuccess, postFormError ,getTodosSuccess,getTodosError } from '../reducers/Form'
const URL = 'http://localhost:8000/api/'


function * getTodos(){
  try{
    const result = yield axios.get(URL + 'todos', { params : { 
      page : 0,
      pageSize : 30
        
    } }).then(r => r)
    
    
    console.log(result);
    yield put(getTodosSuccess(result.data))
  }
  catch(error){
    yield put(getTodosError('Hubo un error'))


  }
}
//({ type :'SAVE_FORM', data })
function * postForm(action){
  try{
    const data = action.data
    data.key = 'esto es un token'
    const result = yield axios.post(URL + 'todos', data).then(r => r)
    yield delay(3000)
    yield put(postFormSuccess(result))
   }
   catch(e){
     console.log(e);
     yield delay(3000)
     yield put(postFormError('Hubo un error'))
   }
}

function * getTodosSaga(){
   yield takeLatest('GET_TODOS',getTodos)
}
function * postFormSaga(){
   yield takeLatest('SAVE_FORM',postForm)
}


export default  [
  getTodosSaga,
  postFormSaga,
]
