import React from 'react'

const MyInput = ({ name, error, onChange,label }) => {
    return (
      <div>
          <label>{label}</label>
          <input name={name} type='text' onChange={onChange}  />
          {
            error && 
            (<label style={{color: 'red'}}>{error}</label>)
          }
          <br/>
      </div>
    )  
}

export default MyInput