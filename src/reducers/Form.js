
//action


const GET_TODOS = 'GET_TODOS'
const GET_TODOS_SUCCESS = 'GET_TODOS_SUCCESS'
const GET_TODOS_ERROR = 'GET_TODOS_FAIL'

const SAVE_FORM = 'SAVE_FORM'
const SAVE_FORM_SUCCESS = 'SAVE_FORM_SUCCESS'
const SAVE_FORM_FAILURE = 'SAVE_FORM_FAILURE'


export const sendForm = (data) => ({ type :SAVE_FORM, data})

export const getTodos = () => ({ type : GET_TODOS })
export const getTodosSuccess = (todos) => ({ type : GET_TODOS_SUCCESS , todos })
export const getTodosError = (error) => ({ type : GET_TODOS_ERROR , error })



export const postFormSuccess = (result) => ({ type : SAVE_FORM_SUCCESS, result })
export const postFormError  =  (error) => ({ type :  SAVE_FORM_FAILURE , error})


const initialState = {
    nombre : 'Ricardo',
    apellido : '',
    error: null,
    cargando : false,
    todos: []
}

const  formReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TODOS_SUCCESS:
      return  { ...state, todos: action.todos }
      
    case GET_TODOS_ERROR:
      return { ...state, todos: [], error: action.error }
      
      
    case SAVE_FORM_SUCCESS:
        return  { ...state, result : action.result , error : null , cargando :false }
    case SAVE_FORM_FAILURE:
        return  { ...state, result : null , error : action.error ,cargando :false}
    case SAVE_FORM:
      return  { ...state, ...action.data , cargando : true}
    default:
      return  state
  }
  return state
}

export default  formReducer
