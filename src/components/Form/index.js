import React from 'react'
import { sendForm, getTodos } from '../../reducers/Form'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import MyInput from './input'

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    sendForm,
    getTodos,
  }, dispatch)
}


const mapStateToProps = (state) => {
  return {
      cargando : state.formReducer.cargando,
      todos : state.formReducer.todos,
      error : state.formReducer.error
  }
}


class Form extends React.Component {


    constructor(props){
      super(props)
      this.state = {
        nombre: '',
        apellido : '',
        dirrecion: '',
        
        nombreError: null,
        apellidoError : null,
        dirrecionError: null,
      
      }
    }
    

    //Antes del primer render
    componentWillMount(){
        this.props.getTodos()
    }


    //Despues del primer render
    /*componentDidMount(){

    }*/

    //Se reciben nueva propiedades
    componentWillReceiveProps(nextProps){

    }


    //Se decide si se aplica la actulizacion
    /*shouldComponentUpdate(nextProps, nextState){
      return super.shouldComponentUpdate()
    }*/

    //Component ya tiene nueva props
    componentWillUpdate(nextProps, nextState){
      console.log('Props antiguas')
      console.log(this.props);
      console.log('State antiguo')
      console.log(this.state);

      console.log('Props nuevas')
      console.log(nextProps);
      console.log('State nuevo')
      console.log(nextState);



    }

    //Component ya se actulizo
    componentDidUpdate(prevProps, prevState){

    }

    //El component cacho una excepcion
    componentDidCatch(error, info){
    }

    onChange = (e) =>{
      this.validate(e.target.name, e.target.value)
      //this.setState({
      //    [e.target.name] : e.target.value
      // })
    }
    
    
    
    validate = (field, value) => {
      if(field === 'nombre') {
        const MIN_LENGTH_NOMBRE = 4
        if(value === '' || value.length === 0){
          this.setState({ 
            nombreError : `El campo ${field} no puede ser vacio`
          })
          return
        }
        if(value.length < MIN_LENGTH_NOMBRE){
          this.setState({ 
            nombreError : `El campo ${field} no puede tener menos de ${MIN_LENGTH_NOMBRE} letras`
          })
          return
        }
        return this.setState({ [field] : value, [`${field}Error`] : null})
      }
      if(field === 'dirrecion'){
        if(value === '' || value.length === 0){
          this.setState({ 
            dirrecionError : `El campo ${field} no puede ser vacio`
          })
          return this.setState({ dirrecion : value, dirrecionError : null})
        }
      }
    }

    onSubmit = (e) => {
      e.preventDefault()
      this.props.sendForm(this.state)
    }

    pintarTodo = (todo) => {
      return (
         <div>
           <label>{todo.name}</label>
           <label>{todo.date}</label>
        </div>
      )
    }

    render(){
      const todos = this.props.todos
      return (
        <div>
          <form>
            <label>Formulario</label>
            <fieldset>
              {
                todos.map(todo => this.pintarTodo(todo))
              }
              {
                this.props.error && 
                (
                  <div>
                      <label style={{ color : 'red' }}>{this.props.error}</label>
                      <br/>
                  </div>
                )
              }
              {
                this.props.cargando ?
                (
                  <div>
                      <label style={{ color : 'green'}}>cargando</label>
                      <br/>
                  </div>
                )
                :
                (
                <div>
                  <MyInput 
                        label={'Nombre'}
                        name={'nombre'}
                        onChange={this.onChange}
                        error={this.state.nombreError}  
                          />
                  <MyInput 
                        label={'Apellido'}
                       name={'apellido'}
                       onChange={this.onChange}
                        error={this.state.apellidoError}  
                                  />

                  <MyInput 
                          label={'Dirrecion'}
                          name={'dirrecion'}
                          onChange={this.onChange}
                          error={this.state.dirrecionError}  
                              />
                </div>
              )
            }
            </fieldset>
            <button type='submit' onClick={this.onSubmit}>Enviar</button>
          </form>
          <label>Valores</label>
          <p>{
            JSON.stringify(this.state,null,' ')
          }</p>
        </div>
      )
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Form)
